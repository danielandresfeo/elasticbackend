<?php
declare(strict_types=1);
header('Access-Control-Allow-Origin: *');  
header("Access-Control-Allow-Headers: *");
use App\Application\Actions\User\ListUsersAction;
use App\Application\Actions\User\ViewUserAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;
use Elasticsearch\ClientBuilder;

return function (App $app) {

   
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('Hello world!');
        return $response;
    });


    $app->post('/crear', function (Request $request, Response $response) {
        $hosts = [
            '127.0.0.1:9200' 
        ];
        $client = Elasticsearch\ClientBuilder::create()->setHosts($hosts)->build();    
    
        $params = [
            'index' => 'personas',
            //'id'    => '4',
            'body'  => [
                        'cedula' => $request->getParsedBody()['cedula'],
                        'nombre' =>  $request->getParsedBody()['nombre'],
                        'apellido' => $request->getParsedBody()['apellido']
                        ]
        ];
        $client->index($params);
    
        sleep(1);
        $params = [
            'index' => 'personas'
        ];
        $result = $client->search($params);
        $response->getBody()->write(json_encode($result));
        return $response
                  ->withHeader('Content-Type', 'application/json');
    });

    $app->post('/editar', function (Request $request, Response $response) {
        
    $hosts = [
        '127.0.0.1:9200' 
     ];
    $client = Elasticsearch\ClientBuilder::create()->setHosts($hosts)->build();    
    
        $params = [
            'index' => 'personas',
            'id'    =>   $request->getParsedBody()['id'],
            'body'  => [
                'doc' => [
                        'cedula' => $request->getParsedBody()['cedula'],
                        'nombre' =>  $request->getParsedBody()['nombre'],
                        'apellido' => $request->getParsedBody()['apellido']
                    ]
            ]
        ];
        $client->update($params);
        //echo json_encode($response);
        $params = [
            'index' => 'personas'
        ];
        sleep(1);
        $result = $client->search($params);
        $response->getBody()->write(json_encode($result));
        return $response
                  ->withHeader('Content-Type', 'application/json');
    });

    $app->post('/borrar', function (Request $request, Response $response) {
    
        $hosts = [
            '127.0.0.1:9200' 
         ];
        $client = Elasticsearch\ClientBuilder::create()->setHosts($hosts)->build();    
        
        $params = [
            'index' => 'personas',
            'id'    => $request->getParsedBody()['id']
        ];
        $client->delete($params);
        //echo json_encode($result);
        $params = [
            'index' => 'personas'
        ];
        sleep(1);
        $result = $client->search($params);
        $response->getBody()->write(json_encode($result));
        return $response
                  ->withHeader('Content-Type', 'application/json');
    });

    $app->get('/listar', function (Request $request, Response $response) {
        
    $hosts = [
        '127.0.0.1:9200' 
     ];
    $client = Elasticsearch\ClientBuilder::create()->setHosts($hosts)->build();    
    
        $params = [
            'index' => 'personas'
        ];
        $result = $client->search($params);
        $response->getBody()->write(json_encode($result));
        return $response
                  ->withHeader('Content-Type', 'application/json');
    });


    $app->group('/users', function (Group $group) {
        $group->get('', ListUsersAction::class);
        $group->get('/{id}', ViewUserAction::class);
    });
};
